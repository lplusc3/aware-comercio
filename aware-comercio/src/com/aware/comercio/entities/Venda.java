package com.aware.comercio.entities;

import java.time.LocalDate;

public class Venda{
    private int numeroDaVenda;
    private String nomeDoCliente;
    private LocalDate dataDaVenda;
    private double valorDaVenda;
    
    public Venda(int numeroDaVenda,String nomeDoCliente,LocalDate dataDaVenda,double valorDaVenda) {
        this.numeroDaVenda = numeroDaVenda;
        this.nomeDoCliente = nomeDoCliente;
        this.dataDaVenda = dataDaVenda;
        this.valorDaVenda = valorDaVenda;
    }
    
    public int getNumeroDaVenda() {
        return this.numeroDaVenda;
    }
    
    public String getNomeDoCliente() {
        return this.nomeDoCliente;
    }
    
    public LocalDate getDataDaVenda() {
        return this.dataDaVenda;
    }
    
    public double getValorDaVenda() {
        return this.valorDaVenda;
    }
}
