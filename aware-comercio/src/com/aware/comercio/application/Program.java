package com.aware.comercio.application;

import com.aware.comercio.utility.HandleFile;
import com.aware.comercio.utility.Pathname;

public class Program
{
    public static void main(final String[] args) {
        Pathname pathname = new Pathname();
        String[] strs = pathname.getFileName();
        HandleFile file = new HandleFile();
        file.executableMoveFile(strs);
    }
}
