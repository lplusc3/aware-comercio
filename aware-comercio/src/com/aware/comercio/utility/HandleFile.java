package com.aware.comercio.utility;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import com.aware.comercio.entities.Venda;

public class HandleFile
{
    public void handleFile(String path, List<String> arr) {
        try {
            Throwable t = null;
            try {
                BufferedReader br = new BufferedReader(new FileReader(path));
                try {
                    String line = br.readLine();
                    while (line != null) {
                        line = br.readLine();
                        line = br.readLine();
                        if (line != null) {
                            arr.add(line);
                        }
                    }
                }
                finally {
                    if (br != null) {
                        br.close();
                    }
                }
            }
            finally {
                if (t == null) {
                    Throwable exception = null;
                    t = exception;
                }
                else {
                     Throwable exception = null;
                    if (t != exception) {
                        t.addSuppressed(exception);
                    }
                }
            }
        }
        catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
    
    public boolean moveFile(String sourcePath, String targetPath) {
        boolean fileMoved = true;
        try {
            Files.move(Paths.get(sourcePath, new String[0]), Paths.get(targetPath, new String[0]), StandardCopyOption.REPLACE_EXISTING);
        }
        catch (Exception e) {
            fileMoved = false;
            e.printStackTrace();
        }
        return fileMoved;
    }
    
    public void executableMoveFile(String[] arrOfStrings) {
        for (String s : arrOfStrings) {
            String path = "PENDENTES/" + s;
            List<String> strings = new ArrayList<String>();
            List<Venda> vendas = new ArrayList<Venda>();
            HandleStrings handleStrings = new HandleStrings();
            boolean flag = true;
            this.handleFile(path, strings);
            if (strings.isEmpty()) {
                flag = false;
            }
            handleStrings.handleString(strings, vendas);
            if (vendas.size() != strings.size()) {
                flag = false;
            }
            if (flag) {
                final String destination = "VALIDADO/" + s;
                this.moveFile(path, destination);
            }
            else {
                final String destination = "INVALIDADO/" + s;
                this.moveFile(path, destination);
            }
        }
    }
}
