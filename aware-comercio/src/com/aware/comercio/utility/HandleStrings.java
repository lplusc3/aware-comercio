package com.aware.comercio.utility;

import java.time.LocalDate;
import java.util.List;

import com.aware.comercio.entities.Venda;

public class HandleStrings
{
    public void handleString(List<String> strings,List<Venda> vendas) {
        for (String str : strings) {
            String[] arr = str.split(";");
            if (arr.length < 4) {
                return;
            }
            int numero = Integer.valueOf(arr[0]);
            String nome = arr[1];
            String[] datas = arr[2].split("/");
            LocalDate data = LocalDate.of(Integer.valueOf(datas[2]), Integer.valueOf(datas[1]), Integer.valueOf(datas[0]));
            double valor = Double.valueOf(arr[3].replace(",", "."));
            vendas.add(new Venda(numero, nome, data, valor));
        }
    }
}
