# aware-comercio

## Aware Comércio

O projeto contêm o executável jar, e as pastas pra onde os arquivos irão ser movidos. A pasta PENDENTES possui 3 arquivos: 1 arquivos vazio, um com tudo que é necessário para atender os requisitos, e um com alguns campos em falta, sendo assim ao executar o jar, 2 arquivos devem ser movidos para a pasta INVALIDADO e um para a pasta VALIDADO

``` 
git clone https://gitlab.com/lplusc3/aware-comercio.git
```
```
unzip aware-comercio-main.zip
```
```
cd aware-comercio-main
```
```
java -jar awarecomercio.jar
```
